var fs = require('fs');
let jsonFile = fs.readFileSync('./data/MOCK_DATA.json', 'utf-8')
let jsonMock = JSON.parse(jsonFile);
const DB = require('better-sqlite3-helper');

// This is an example of importing data into the database, 
// given an JSON file in this example. It is a good way to get 
// in mock data, but with changing the input to suit your Space's 
// needs you can get a nice initial dataset ready to global. 
// I would imagine you could import a csv dataset pretty easily 
// with this method too.

DB({
    path: './data/sqlitedb.db', // this is the default
    memory: false, // create a db only in memory
    readonly: false, // read only
    fileMustExist: false, // throw error if database not exists
    WAL: true, // automatically enable 'PRAGMA journal_mode = WAL'
    migrate: {  // disable completely by setting `migrate: false`
      force: 'last', // set to 'last' to automatically reapply the last migration-file
      table: 'migration', // name of the database table that is used to keep track
      migrationsPath: './migrations' // path of the migration-files
    }
  }) 

console.log("Hello! we're about to populate the data in here. we'll use MOCK_DATA.json");

console.log(`About to insert ${ Object.keys(jsonMock).length } entries..`);
  jsonMock.forEach(element => { 
      DB().insert('parts', {
        "data": JSON.stringify(element)
    });
     
  })
console.log(`Success! Finished.`);

