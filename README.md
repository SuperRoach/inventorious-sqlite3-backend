# Inventorious SQLite3 backend

Backend that makes use of Express, better-sqlite3-helper to make it speedy to get a database running with exposed API to make use of it.

Want to start from scratch with your database? a few options
- Start with the sqlitedb-blank.db (and renaming it to sqlite.db) 
- Look at running `node add-mock-data.js`. It's an example of populating a set of data quickly, and would be easy to modify or import something you already have that you can transform.
- If you delete all the *.db files in /data, the next time you run add-mock-data.js it will regenerate a blank file, given the .sql file in /migrations. It will error for safety reasons if you attempt to run the backend in this state.